<?php

/**
 * function that checks if logs exist
 * @return type
 */
function check_log_exists() {
    $install_path = '/var/www/wpclean';
    $dir = $install_path . '/Apache_logs/temp_logs/';
    if (!is_readable($dir)) {
        return NULL;
    }
    if ((count(scandir($dir)) > 2)) {
        return scandir($dir);
    }
}

/**
 * Function that fetches logs after they have been imported locally
 * @param type $log
 * @return type
 */
function open_logs($log) {
    //@TO DO: add variables 
    $install_path = '/var/www/wpclean';
    $dir = $install_path . '/Apache_logs/temp_logs/' . $log;

    //create logs array
    $logs_array = array();
    $handle = fopen($dir, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $logs_array[] = $line;
        }
        fclose($handle);
        return $logs_array;
    } else {
        return array();
    }
}

/**
 * generate markup for entire table
 * @param type $all_logs
 * @return string
 */
function generate_markup($all_logs) {
    if (!empty($all_logs)) {
        $body = '';
        $title ='';
        foreach ($all_logs as $key => $value) {
            if (!empty($value)) {
                $body_array = body_markup($value,$key);
                $title = header_markup($key,$value,$body_array['form']);
                foreach ($body_array as $one) {
                    if(!empty($one['body'])){
                    $body .= implode('', $one['body']);
                }
                }
            }
                      //  $markup_element[] = ;
            $markup_element[] = '<table style="width:100%">' . $title . $body . '</table> <br/>';

        }
        return $markup_element ;
    }
    return '';
}

/**
 * generate header for table
 * @param type $title
 * @return type
 */
function header_markup($title,$value,$form) {
    $output = '';
    if (!empty($title) && !empty($value)) {
       $offender = calculate_offence($value); 
       
        $output ='<tr>'; 
        $output .= '<th><strsong><p>' . $title . '</p></strong></th>';
        $output .= '<th><strsong><p>Most offences ' . $offender['ip'] . $form . '</p></strong></th>';
        $output .= '<th><strsong><p>Number of accesses ' . $offender['offences'] . '/' .$offender['total_ips'] . '</p></strong></th>';
        $output .= '</tr>';
       // atach_report($line);
    }
    return $output;
}

/**
 * return markup for the body
 * @param type $value_array
 * @return string
 */
function body_markup($value_array,$project) {
    if (!empty($value_array) && !empty($project)) {
        $markup = array();
        //iterator
        $i = 0;
        $all_ips = array();
        foreach ($value_array as $one_log) {
           $ip = explode("- -", $one_log, 2);
           $all_ips[] = $ip[0]; 
                //extract ip
                
                $markup[$i]['body']['prefix'] = '<tr>';
                $markup[$i]['body']['ip'] = '<td><p class ="ip">' . $ip[0] . '</p></td>';

                //extract time
                $markup[$i]['body']['time'] = '<td><p class ="time">' . get_string_between($one_log, "[", "]") . '</p></td>';
                //extract data
                $markup[$i]['body']['full_data'] = '<td><p class ="data">' . $one_log . '</p></td>';
                $markup[$i]['body']['ip'] = '<td><p class ="ip">' . $ip[0] . '</p></td>';

                $markup[$i]['body']['sufix'] = '</tr>';
                //add 1 to array
                $i++; 
        }
        $markup['form'] = generate_form($all_ips,$project);
        return $markup;
    }
    return array();
}

/**
 * Get value of string between start and end values
 * @param string $string
 * @param type $start
 * @param type $end
 * @return string
 */
function get_string_between($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0)
        return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function render_markup($all_logs){
    $markup_aray = generate_markup($all_logs);
    foreach($markup_aray as $one){
        print $one;
    }
}

/**
 * Calculate Ip with most accesses
 * @param array $value
 * @return type
 */
function calculate_offence($value) {
    if (!empty($value)) {
        $offender_array = array();
        $total = 0;
        foreach ($value as $one) {
            $ip = explode("- -", $one, 2);
            $offender_array[$ip[0]][] = $ip[0];
            if(!empty($ip[0])){
                $total++;
            }
        }
        foreach ($offender_array as $key => $value) {
            $offender_array[$key] = count($value);
        }
        $top_offender = array_search(max($offender_array), $offender_array); // john

        return array('ip' => $top_offender,
            'offences' => $offender_array[$top_offender],
            'total_ips' => $total,);
    }
    return array();
}

/**
 * add basic style to generated table
 */
function generate_css(){
    print '<style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 15px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color: #fff;
}
table#t01 th {
    background-color: black;
    color: white;
}
</style>';    
}

function generate_form($ips_array,$project) {
    
    $project = str_replace("-access.log","",$project);
    $top_array = calculate_occurences($ips_array);
    
    $head = '<form action="/Apache_logs/blocker.php" method="post">';
    $foot .= '<input type="hidden" name="project" value="' . $project . '">';
    $foot .= '<input type="submit" value="Submit"></form>';
    $body ='';
    foreach ($top_array as $key => $one_form) {
        $body .= '<input type="checkbox" name="' . $key .'" value="' . $key . '" checked>Block ' . $key . '  <br/>Nr of occurences => ' . $one_form . ' </input> <br/>';
    }
    return $head . $body . $foot;
}

function calculate_occurences($ips) {
    $top_occurences = array_count_values($ips);
    arsort($top_occurences);
    $return_array = array();

    $i = 0;
    foreach ($top_occurences as $key => $occurence_count) {
        if ($i < 5) {
            $return_array[$key] = $occurence_count;
            $i++;
        }
    }
    return $return_array;
}


function generate_main_form(){
    print 
'<center><form action="run_apache.php" method="post">
    <h3>To rerun the log import click the button bellow</h3>
   <input type="submit" value="RERUN LOG CHECKER">
</form></center>';
}







function apache_logs(){
// TO DO : automaticaly run Apache_check sh
//check if we have any new logs
$logs = check_log_exists();

if ($logs) {
    unset($logs[0]);
    unset($logs[1]);
    foreach ($logs as $log) {

        $all_logs[$log] = open_logs($log);
    }
}

generate_css();

render_markup($all_logs);
}


