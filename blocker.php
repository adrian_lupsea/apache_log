<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//IMPLEMENTATION OF WRITE TO HTACCESS
//Generate markup of ofending IPS
function create_offender_markup($offending_ips) {
    $markup = '';

    if (!empty($offending_ips)) {
        $markup .= PHP_EOL . '#Blocked_IPs' . PHP_EOL;
        $markup .= '#Order Deny,Allow' . PHP_EOL;
        foreach ($offending_ips as $ip) {
            $markup .= "#Deny from " . $ip . PHP_EOL;
        }
        return $markup;
    } else {
        return $markup;
    }
}

function write_to_htaccess($project_path, $markup, $write_mode) {
    // write mode "w" or "a+"
    $fp = fopen($project_path . '/.htaccess', $write_mode);
    if ($fp) {
        fwrite($fp, $markup);
        fclose($fp);
    }
}

function detect_htaccess_ips($project_path) {
    $file = file_get_contents($project_path . '/.htaccess', true);
    //remove everything 
    $base_access = substr($file, 0, strpos($file, "#Blocked_IPs"));

    write_to_htaccess($project_path, $base_access, 'w');
}

//run this function per project
function process_htaccess($project, $offenders_array) {
    write_to_htaccess('/var/www/' . $project, '#Blocked_IPs', 'a+');
    //create function to load top offenders
    $offenders_markup = create_offender_markup($offenders_array);

    detect_htaccess_ips('/var/www/' . $project);

    write_to_htaccess('/var/www/' . $project, $offenders_markup, "a+");
}

function blocklist_list_check($project, $offenders) {


    if (file_exists('blocklist/' . $project . '_blocklist.txt')) {
        $file_offenders = array();
        $handle = fopen('blocklist/' . $project . '_blocklist.txt', 'r');
        while (($data = fgetcsv($handle)) !== FALSE) {
            $file_offenders[] = $data;
        }
        fclose($handle);
        if (!empty($file_offenders[0])) {
            $all_array = array_unique(array_merge(array_values($file_offenders[0]), array_values($offenders)));
        } else {
            $all_array = array_values($offenders);
        }

        $handle = fopen('blocklist/' . $project . '_blocklist.txt', 'w');
        fputcsv($handle, $all_array);
        fclose($handle);
        return $all_array;
    } else {
        print 'NO File';
        fopen('blocklist/' . $project . '_blocklist.txt', 'w');
        fclose('blocklist/' . $project . '_blocklist.txt');
        blocklist_list_check($project, $offenders);
    }
}

//Run the script

if (!empty($_POST)) {
    $offenders = $_POST;

foreach($offenders as $key => $value){
    if($key == 'project'){
     $project = $value;
     unset($offenders['project']);
    }
}

    $offenders = blocklist_list_check($project, $offenders);

    process_htaccess($project, $offenders);

    print '<h1>htaccess has been rewriten with submited values</h1><br/>'
    . '<h2><a href=/Apache_logs/log_list.php>Go back to list page</a></h2>';
} else {
    print "No values Selected";
}



//header('Location: ' . $_SERVER['HTTP_REFERER']);

